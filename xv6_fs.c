// File system implementation.  Five layers:
//   + Blocks: allocator for raw disk blocks.
//   + Log: crash recovery for multi-step updates.
//   + Files: inode allocator, reading, writing, metadata.
//   + Directories: inode with special contents (list of other inodes!)
//   + Names: paths like /usr/rtm/xv6/fs.c for convenient naming.
//
// This file contains the low-level file system manipulation
// routines.  The (higher-level) system call implementations
// are in sysfile.c.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "vfs.h"
#include "xv6_fs.h"
#include "buf.h"
#include "file.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
static void xv6_itrunc(struct inode*);
// there should be one superblock per disk device, but we run with
// only one device
struct xv6_super_block sb;

void          xv6_iupdate(struct inode *ip);
void          xv6_itrunc(struct inode *ip);
struct inode* xv6_iget(uint dev, uint inum);
void          xv6_iput(struct inode *ip);
struct inode* xv6_idup(struct inode *ip);
int           xv6_readi(struct inode *ip, char *dst, uint off, uint n);
int           xv6_writei(struct inode *ip, char *src, uint off, uint n);
int           xv6_dirlink(struct inode *dp, char *name, uint inum);
struct inode* xv6_dirlookup(struct inode *dp, char *name, uint *poff);
void          xv6_ilock(struct inode *ip);
void          xv6_iunlock(struct inode *ip);
void          xv6_iunlockput(struct inode *ip);

void xv6_check_iops() {
  cprintf("xv6 iops\n");
}

struct inode_ops xv6_ops = {
  .iupdate = &xv6_iupdate,
  .itrunc = &xv6_itrunc,
  .iget = &xv6_iget,
  .iput = &xv6_iput,
  .idup = &xv6_idup,
  .readi = &xv6_readi,
  .writei = &xv6_writei,
  .dirlink = &xv6_dirlink,
  .dirlookup = &xv6_dirlookup,
  .ilock = &xv6_ilock,
  .iunlock = &xv6_iunlock,
  .iunlockput = &xv6_iunlockput,
  .check_iops = &xv6_check_iops,
};

/**
 * ??
 * Shouldn't we be reading the value of superblock into the global sb variable
 * when doing memmove
 * 
 * If that is what is happening, then better to rename the ptr in args
 */
// Read the super block.
void
xv6_readsb(int dev, struct xv6_super_block *sb)
{
  struct buf *bp;

  bp = bread(dev, 1);
  memmove(sb, bp->data, sizeof(*sb));
  brelse(bp);
}

// Zero a block.
static void
bzero(int dev, int bno)
{
  struct buf *bp;

  bp = bread(dev, bno);
  memset(bp->data, 0, BSIZE);
  log_write(bp);
  brelse(bp);
}

// Blocks.

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
      m = 1 << (bi % 8);
      if((bp->data[bi/8] & m) == 0){  // Is block free?
        bp->data[bi/8] |= m;  // Mark block in use.
        log_write(bp);
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
  bi = b % BPB;
  m = 1 << (bi % 8);
  if((bp->data[bi/8] & m) == 0)
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
  log_write(bp);
  brelse(bp);
}

// Inodes.
//
// An inode describes a single unnamed file.
// The inode disk structure holds metadata: the file's type,
// its size, the number of links referring to it, and the
// list of blocks holding the file's content.
//
// The inodes are laid out sequentially on disk at
// sb.startinode. Each inode has a number, indicating its
// position on the disk.
//
// The kernel keeps a cache of in-use inodes in memory
// to provide a place for synchronizing access
// to inodes used by multiple processes. The cached
// inodes include book-keeping information that is
// not stored on disk: ip->ref and ip->valid.
//
// An inode and its in-memory representation go through a
// sequence of states before they can be used by the
// rest of the file system code.
//
// * Allocation: an inode is allocated if its type (on disk)
//   is non-zero. ialloc() allocates, and xv6_iput() frees if
//   the reference and link counts have fallen to zero.
//
// * Referencing in cache: an entry in the inode cache
//   is free if ip->ref is zero. Otherwise ip->ref tracks
//   the number of in-memory pointers to the entry (open
//   files and current directories). xv6_iget() finds or
//   creates a cache entry and increments its ref; xv6_iput()
//   decrements ref.
//
// * Valid: the information (type, size, &c) in an inode
//   cache entry is only correct when ip->valid is 1.
//   ilock() reads the inode from
//   the disk and sets ip->valid, while xv6_iput() clears
//   ip->valid if ip->ref has fallen to zero.
//
// * Locked: file system code may only examine and modify
//   the information in an inode and its content if it
//   has first locked the inode.
//
// Thus a typical sequence is:
//   ip = xv6_iget(dev, inum)
//   ilock(ip)
//   ... examine and modify ip->xxx ...
//   iunlock(ip)
//   xv6_iput(ip)
//
// ilock() is separate from xv6_iget() so that system calls can
// get a long-term reference to an inode (as for an open file)
// and only lock it for short periods (e.g., in read()).
// The separation also helps avoid deadlock and races during
// pathname lookup. xv6_iget() increments ip->ref so that the inode
// stays cached and pointers to it remain valid.
//
// Many internal file system functions expect the caller to
// have locked the inodes involved; this lets callers create
// multi-step atomic operations.
//
// The icache.lock spin-lock protects the allocation of icache
// entries. Since ip->ref indicates whether an entry is free,
// and ip->dev and ip->inum indicate which i-node an entry
// holds, one must hold icache.lock while using any of those fields.
//
// An ip->lock sleep-lock protects all ip-> fields other than ref,
// dev, and inum.  One must hold ip->lock in order to
// read or write that inode's ip->valid, ip->size, ip->type, &c.

struct icache {
  struct spinlock lock;
  struct inode inodes[NINODE];
  struct xv6_inode xv6_inodes[NINODE];
} icache;

void
iinit(int dev)
{
  int i = 0;
  
  initlock(&icache.lock, "icache");
  for(i = 0; i < NINODE; i++) {
    initsleeplock(&icache.inodes[i].lock, "inode");
  }

  xv6_readsb(dev, &sb);
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
 inodestart %d bmap start %d\n", sb.size, sb.nblocks,
          sb.ninodes, sb.nlog, sb.logstart, sb.inodestart,
          sb.bmapstart);
}

//PAGEBREAK!
// Allocate an inode on device dev.
// Mark it as allocated by  giving it type type.
// Returns an unlocked but allocated and referenced inode.
struct inode*
ialloc(uint dev, short type)
{
  int inum;
  struct buf *bp;
  struct dinode *dip;

  for(inum = 1; inum < sb.ninodes; inum++){
    bp = bread(dev, IBLOCK(inum, sb));
    dip = (struct dinode*)bp->data + inum%IPB;
    if(dip->type == 0){  // a free inode
      memset(dip, 0, sizeof(*dip));
      dip->type = type;
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return xv6_iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}

// Copy a modified in-memory inode to disk.
// Must be called after every change to an ip->xxx field
// that lives on disk, since i-node cache is write-through.
// Caller must hold ip->lock.
void
xv6_iupdate(struct inode *ip)
{
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
  dip = (struct dinode*)bp->data + ip->inum%IPB;

  struct xv6_inode *pxv6_inode = ip->fs_inode;
  dip->type = pxv6_inode->type;
  dip->major = pxv6_inode->major;
  dip->minor = pxv6_inode->minor;
  dip->nlink = pxv6_inode->nlink;
  dip->size = pxv6_inode->size;

  memmove(dip->addrs, pxv6_inode->addrs, sizeof(pxv6_inode->addrs));
  log_write(bp);
  brelse(bp);
}

// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
struct inode*
xv6_iget(uint dev, uint inum)
{
  struct inode *ip, *empty;
  acquire(&icache.lock);


  // Is the inode already cached?
  int inode_num = -1;
  empty = 0;
  for(ip = &icache.inodes[0]; ip < &icache.inodes[NINODE]; ip++){
    if (!empty)
      inode_num++;

    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    
    // Remember empty slot.
    if(empty == 0 && ip->ref == 0)
      empty = ip;
  }


  // Recycle an inode cache entry.
  if(empty == 0)
    panic("xv6_iget: no inodes");

  // empty now contains a reference to the free inode found in cache
  ip = empty;
  
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  ip->iops = &xv6_ops;
  ip->fs_inode = &icache.xv6_inodes[inode_num];

  release(&icache.lock);
  return ip;
}

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
xv6_idup(struct inode *ip)
{
  acquire(&icache.lock);
  ip->ref++;
  release(&icache.lock);
  return ip;
}

// Lock the given inode.
// Reads the inode from disk if necessary.
void
xv6_ilock(struct inode *ip)
{
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquiresleep(&ip->lock);

  if(ip->valid == 0){
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
    dip = (struct dinode*)bp->data + ip->inum%IPB;

    struct xv6_inode *pxv6_inode = ip->fs_inode;
    pxv6_inode->type = dip->type;
    pxv6_inode->major = dip->major;
    pxv6_inode->minor = dip->minor;
    pxv6_inode->nlink = dip->nlink;
    pxv6_inode->size = dip->size;

    memmove(pxv6_inode->addrs, dip->addrs, sizeof(pxv6_inode->addrs));
    brelse(bp);
    ip->valid = 1;
    if(pxv6_inode->type == 0)
      panic("ilock: no type");
  }
}

// Unlock the given inode.
void
xv6_iunlock(struct inode *ip)
{
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
    panic("iunlock");

  releasesleep(&ip->lock);
}

// Drop a reference to an in-memory inode.
// If that was the last reference, the inode cache entry can
// be recycled.
// If that was the last reference and the inode has no links
// to it, free the inode (and its content) on disk.
// All calls to xv6_iput() must be inside a transaction in
// case it has to free the inode.
void
xv6_iput(struct inode *ip)
{
  acquiresleep(&ip->lock);
  struct xv6_inode *pxv6_inode = ip->fs_inode;
  if(ip->valid && pxv6_inode->nlink == 0){
    acquire(&icache.lock);
    int r = ip->ref;
    release(&icache.lock);
    if(r == 1){
      // inode has no links and no other references: truncate and free.
      xv6_itrunc(ip);
      pxv6_inode->type = 0;
      xv6_iupdate(ip);
      ip->valid = 0;
    }
  }
  releasesleep(&ip->lock);

  acquire(&icache.lock);
  ip->ref--;
  release(&icache.lock);
}

// Common idiom: unlock, then put.
void
xv6_iunlockput(struct inode *ip)
{
  iunlock(ip);
  xv6_iput(ip);
}

//PAGEBREAK!
// Inode content
//
// The content (data) associated with each inode is stored
// in blocks on the disk. The first NDIRECT block numbers
// are listed in ip->addrs[].  The next NINDIRECT blocks are
// listed in block ip->addrs[NDIRECT].

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
  uint addr, *a;
  struct buf *bp;
  struct xv6_inode *pxv6_inode = (ip->fs_inode);

  if(bn < NDIRECT){
    if((addr = pxv6_inode->addrs[bn]) == 0)
      pxv6_inode->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;

  if(bn < NINDIRECT){
    // Load indirect block, allocating if necessary.
    if((addr = pxv6_inode->addrs[NDIRECT]) == 0)
      pxv6_inode->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
      a[bn] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}

// Truncate inode (discard contents).
// Only called when the inode has no links
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
xv6_itrunc(struct inode *ip)
{
  int i, j;
  struct buf *bp;
  uint *a;
  struct xv6_inode *pxv6_inode = ip->fs_inode;


  for(i = 0; i < NDIRECT; i++){
    if(pxv6_inode->addrs[i]){
      bfree(ip->dev, pxv6_inode->addrs[i]);
      pxv6_inode->addrs[i] = 0;
    }
  }

  if(pxv6_inode->addrs[NDIRECT]){
    bp = bread(ip->dev, pxv6_inode->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
    bfree(ip->dev, pxv6_inode->addrs[NDIRECT]);
    pxv6_inode->addrs[NDIRECT] = 0;
  }

  pxv6_inode->size = 0;
  xv6_iupdate(ip);
}


//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
xv6_readi(struct inode *ip, char *dst, uint off, uint n)
{
  uint tot, m;
  struct buf *bp;
  struct xv6_inode *pxv6_inode = ip->fs_inode;

  if(pxv6_inode->type == T_DEV){
    if(pxv6_inode->major < 0 || pxv6_inode->major >= NDEV || !devsw[pxv6_inode->major].read)
      return -1;
    return devsw[pxv6_inode->major].read(ip, dst, n);
  }

  // second condition is always false !?
  // if(off > pxv6_inode->size || off + n < off)
  if(off > pxv6_inode->size)
    return -1;
  if(off + n > pxv6_inode->size)
    n = pxv6_inode->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
}

// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
xv6_writei(struct inode *ip, char *src, uint off, uint n)
{
  uint tot, m;
  struct buf *bp;
  struct xv6_inode *pxv6_inode = ip->fs_inode;

  if(pxv6_inode->type == T_DEV){
    if(pxv6_inode->major < 0
      || pxv6_inode->major >= NDEV
      || !devsw[pxv6_inode->major].write)
      return -1;
    return devsw[pxv6_inode->major].write(ip, src, n);
  }

  if(off > pxv6_inode->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > pxv6_inode->size){
    pxv6_inode->size = off;
    xv6_iupdate(ip);
  }
  return n;
}

//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
  return strncmp(s, t, DIRSIZ);
}

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
xv6_dirlookup(struct inode *dir_ip, char *name, uint *poff)
{
  uint off, inum;
  struct dirent de;
  struct xv6_inode *pxv6_inode = dir_ip->fs_inode;

  if(pxv6_inode->type != T_DIR)
    panic("xv6dirlookup not DIR");

  for(off = 0; off < pxv6_inode->size; off += sizeof(de)){
    if(xv6_readi(dir_ip, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("xv6dirlookup read");
    if(de.inum == 0)
      continue;
    if(namecmp(name, de.name) == 0){
      // entry matches path element
      if(poff)
        *poff = off;
      inum = de.inum;
      return xv6_iget(dir_ip->dev, inum);
    }
  }

  return 0;
}

// Write a new directory entry (name, inum) into the directory dp.
int
xv6_dirlink(struct inode *dir_ip, char *name, uint inum)
{
  int off;
  struct dirent de;
  struct inode *ip;
  struct xv6_inode *pxv6_inode = dir_ip->fs_inode;

  // Check that name is not present.
  if((ip = xv6_dirlookup(dir_ip, name, 0)) != 0){
    xv6_iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < pxv6_inode->size; off += sizeof(de)){
    if(xv6_readi(dir_ip, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("xv6_dirlink read");
    if(de.inum == 0)
      break;
  }

  strncpy(de.name, name, DIRSIZ);
  de.inum = inum;
  if(xv6_writei(dir_ip, (char*)&de, off, sizeof(de)) != sizeof(de))
    panic("xv6_dirlink");

  return 0;
}
