#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "vfs.h"
#include "xv6_fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

char buf[8192];
char name[3];
char *echoargv[] = { "echo", "ALL", "TESTS", "PASSED", 0 };
int stdout = 1;

void dirtest(void)
{
  printf(stdout, "mkdir test\n");

  if(mkdir("/mnt/dir0") < 0){
    printf(stdout, "mkdir failed\n");
    exit();
  }

  printf(stdout, "dir created\n");

  if(chdir("/mnt/dir0") < 0){
    printf(stdout, "chdir dir0 failed\n");
    exit();
  }

  printf(stdout, "dir entered\n");

  // if(mkdir("/mnt/dir0/subdir0") < 0){
  if(mkdir("subdir0") < 0){
    printf(stdout, "mkdir failed\n");
    exit();
  }

  if(chdir("subdir0") < 0){
    printf(stdout, "chdir subdir0 failed\n");
    exit();
  }

  if(mkdir("subsubdir0") < 0){
    printf(stdout, "mkdir subsubdir0 failed\n");
    exit();
  }

  // if(unlink("subsubdir0") < 0){
  //   printf(stdout, "unlink dir0 failed\n");
  //   exit();
  // }

  // if(chdir("/mnt/dir0/..") < 0){
  if(chdir("..") < 0){
    printf(stdout, "chdir .. failed\n");
    exit();
  }

  // if(unlink("subdir0") < 0){
  //   printf(stdout, "unlink dir0 failed\n");
  //   exit();
  // }

  // if(chdir("/mnt/dir0/..") < 0){
  if(chdir("..") < 0){
    printf(stdout, "chdir .. failed\n");
    exit();
  }

  printf(stdout, "dir left\n");

  // if(unlink("dir0") < 0){
  //   printf(stdout, "unlink dir0 failed\n");
  //   exit();
  // }

  printf(stdout, "mkdir test ok\n");
  exit();
}