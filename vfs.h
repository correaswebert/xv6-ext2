#include "types.h"
#include "sleeplock.h"

#ifndef VFS_H_
#define VFS_H_

#define BSIZE 2048
#define EXT2_FSSIZE 15360

// in-memory copy of an inode
struct inode {
  uint dev;           // Device number
  uint inum;          // Inode number
  int ref;            // Reference count
  struct sleeplock lock; // protects everything below here
  int valid;          // inode has been read from disk?
  void *fs_inode;
  struct inode_ops *iops;
};

/**
 * Pointers to file system specific operations
 */
struct inode_ops {
  void          (*iupdate)(struct inode *ip);
  void          (*itrunc)(struct inode *ip);
  struct inode* (*iget)(uint dev, uint inum);
  void          (*iput)(struct inode *ip);
  struct inode* (*idup)(struct inode *ip);
  void          (*ilock)(struct inode *ip);
  void          (*iunlock)(struct inode *ip);
  void          (*iunlockput)(struct inode *ip);
  int           (*readi)(struct inode *ip, char *dst, uint off, uint n);
  int           (*writei)(struct inode *ip, char *src, uint off, uint n);
  int           (*dirlink)(struct inode *dp, char *name, uint inum);
  struct inode* (*dirlookup)(struct inode *dp, char *name, uint *poff);
  void          (*check_iops)();
};

struct namex_return {
  struct inode *ip;
  int is_ext2;
};

ushort get_inode_type(struct inode *ip);
short update_link_count(struct inode *ip, short update_val);

#endif /* VFS_H_ */

      