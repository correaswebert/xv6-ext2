#ifndef EXT2_FS_H_
#define EXT2_FS_H_

typedef unsigned char   uchar;
typedef unsigned short  ushort;
typedef unsigned int    uint;
typedef unsigned char   uint8;
typedef unsigned short  uint16;
typedef unsigned int    uint32;
typedef unsigned long   uint64;
typedef unsigned int    pde_t;
typedef unsigned long   ext2_fsblk_t;

#define EXT2_MIN_BLKSIZE 1024
#define EXT2_SUPER_MAGIC 0xEF53

#define EXT2_MAX_BGC 40

#define EXT2_NAME_LEN               255
#define EXT2_DIR_PAD                4
#define EXT2_DIR_ROUND              (EXT2_DIR_PAD - 1)
#define EXT2_DIR_REC_LEN(name_len)  (((name_len) + 8 + EXT2_DIR_ROUND) & \
                                     ~EXT2_DIR_ROUND)
#define EXT2_MAX_REC_LEN            ((1<<16)-1)
#define EXT2_MIN_BLOCK_SIZE         1024
#define EXT2_MAX_BLOCK_SIZE         4096
#define EXT2_BLOCK_SIZE(s)          ((s)->blocksize)
#define EXT2_ADDR_PER_BLOCK(s)      (EXT2_BLOCK_SIZE(s) / sizeof (uint32))
#define EXT2_BLOCK_SIZE_BITS(s)     ((s)->s_blocksize_bits)

struct ext2_super_block {
  uint32 s_inodes_count;        /* Inodes count */
  uint32 s_blocks_count;        /* Blocks count */
  uint32 s_r_blocks_count;      /* Reserved blocks count */
  uint32 s_free_blocks_count;   /* Free blocks count */
  uint32 s_free_inodes_count;   /* Free inodes count */
  uint32 s_first_data_block;    /* First Data Block */
  uint32 s_log_block_size;      /* Block size */
  uint32 s_log_frag_size;       /* Fragment size */
  uint32 s_blocks_per_group;    /* # Blocks per group */
  uint32 s_frags_per_group;     /* # Fragments per group */
  uint32 s_inodes_per_group;    /* # Inodes per group */
  uint32 s_mtime;               /* Mount time */
  uint32 s_wtime;               /* Write time */
  uint16 s_mnt_count;           /* Mount count */
  uint16 s_max_mnt_count;       /* Maximal mount count */
  uint16 s_magic;               /* Magic signature */
  uint16 s_state;               /* File system state */
  uint16 s_errors;              /* Behaviour when detecting errors */
  uint16 s_minor_rev_level;     /* minor revision level */
  uint32 s_lastcheck;           /* time of last check */
  uint32 s_checkinterval;       /* max. time between checks */
  uint32 s_creator_os;          /* OS */
  uint32 s_rev_level;           /* Revision level */
  uint16 s_def_resuid;          /* Default uid for reserved blocks */
  uint16 s_def_resgid;          /* Default gid for reserved blocks */

  /*
   * These fields are for EXT2_DYNAMIC_REV superblocks only.
   *
   * Note: the difference between the compatible feature set and
   * the incompatible feature set is that if there is a bit set
   * in the incompatible feature set that the kernel doesn't
   * know about, it should refuse to mount the filesystem.
   *
   * e2fsck's requirements are more strict; if it doesn't know
   * about a feature in either the compatible or incompatible
   * feature set, it must abort and not try to meddle with
   * things it doesn't understand...
   */
  uint32 s_first_ino;               /* First non-reserved inode */
  uint16 s_inode_size;              /* size of inode structure */
  uint16 s_block_group_nr;          /* block group # of this superblock */
  uint32 s_feature_compat;          /* compatible feature set */
  uint32 s_feature_incompat;        /* incompatible feature set */
  uint32 s_feature_ro_compat;       /* readonly-compatible feature set */
  uint8  s_uuid[16];                /* 128-bit uuid for volume */
  char   s_volume_name[16];         /* volume name */
  char   s_last_mounted[64];        /* directory where last mounted */
  uint32 s_algorithm_usage_bitmap;  /* For compression */

  /*
   * Performance hints.  Directory preallocation should only
   * happen if the EXT2_COMPAT_PREALLOC flag is on.
   */
  uint8  s_prealloc_blocks;       /* Nr of blocks to try to preallocate*/
  uint8  s_prealloc_dir_blocks;   /* Nr to preallocate for dirs */
  uint16 s_padding1;

  /*
   * Journaling support valid if EXT3_FEATURE_COMPAT_HAS_JOURNAL set.
   */
  uint8  s_journal_uuid[16];    /* uuid of journal superblock */
  uint32 s_journal_inum;        /* inode number of journal file */
  uint32 s_journal_dev;         /* device number of journal file */
  uint32 s_last_orphan;         /* start of list of inodes to delete */
  uint32 s_hash_seed[4];        /* HTREE hash seed */
  uint8  s_def_hash_version;    /* Default hash version to use */
  uint8  s_reserved_char_pad;
  uint16 s_reserved_word_pad;
  uint32 s_default_mount_opts;
  uint32 s_first_meta_bg;       /* First metablock block group */
  uint32 s_reserved[190];       /* Padding to the end of the block */
};

#define EXT2_NDIR_BLOCKS  12
#define EXT2_IND_BLOCK    EXT2_NDIR_BLOCKS
#define EXT2_DIND_BLOCK   (EXT2_IND_BLOCK + 1)
#define EXT2_TIND_BLOCK   (EXT2_DIND_BLOCK + 1)
#define EXT2_N_BLOCKS     (EXT2_TIND_BLOCK + 1)


/*
 * Structure of an inode on the disk
 */
struct ext2_inode {
  uint16	i_mode;		                  /* File mode */
	uint16	i_uid;		                  /* Low 16 bits of Owner Uid */
	uint32	i_size;		                  /* Size in bytes */
	uint32	i_atime;	                  /* Access time */
	uint32	i_ctime;	                  /* Inode change time */
  uint32	i_mtime;	                  /* Modification time */
	uint32	i_dtime;	                  /* Deletion Time */
	uint16	i_gid;		                  /* Low 16 bits of Group Id */
	uint16	i_links_count;	            /* Links count */
	uint32	i_blocks;	                  /* Blocks count */
  uint32	i_flags;	                  /* File flags */

	union {
		struct {
			uint32	l_i_version;            /* was l_i_reserved1 */
		} linux1;
		struct {
			uint32  h_i_translator;
		} hurd1;
	} osd1;				                      /* OS dependent 1 */

  uint32	i_block[EXT2_N_BLOCKS];     /* Pointers to blocks */
  uint32	i_generation;	              /* File version (for NFS) */
	uint32	i_file_acl;	                /* File ACL */
	uint32	i_size_high;
  uint32	i_faddr;	                  /* Fragment address */

	union {
		struct {
			uint16	l_i_blocks_hi;
			uint16	l_i_file_acl_high;
			uint16	l_i_uid_high;	          /* these 2 fields    */
			uint16	l_i_gid_high;	          /* were reserved2[0] */
			uint16	l_i_checksum_lo;        /* crc32c(uuid+inum+inode) */
			uint16	l_i_reserved;
		} linux2;

		struct {
			uint8	h_i_frag;	                /* Fragment number */
			uint8	h_i_fsize;	              /* Fragment size */
			uint16	h_i_mode_high;
			uint16	h_i_uid_high;
			uint16	h_i_gid_high;
			uint32	h_i_author;
		} hurd2;
	} osd2;				                      /* OS dependent 2 */
};

struct ext2_group_desc
{
	uint32	bg_block_bitmap;	        /* Blocks bitmap block */
	uint32	bg_inode_bitmap;	        /* Inodes bitmap block */
	uint32	bg_inode_table;		        /* Inodes table block */
	uint16	bg_free_blocks_count;	    /* Free blocks count */
	uint16	bg_free_inodes_count;	    /* Free inodes count */
	uint16	bg_used_dirs_count;	      /* Directories count */
	uint16	bg_flags;
	uint32	bg_exclude_bitmap_lo;	    /* Exclude bitmap for snapshots */
	uint16	bg_block_bitmap_csum_lo;  /* crc32c(s_uuid+grp_num+bitmap) LSB */
	uint16	bg_inode_bitmap_csum_lo;  /* crc32c(s_uuid+grp_num+bitmap) LSB */
	uint16	bg_itable_unused;	        /* Unused inodes count */
	uint16	bg_checksum;		          /* crc16(s_uuid+group_num+group_desc)*/
};

#define EXT2_NAME_LEN 255

struct ext2_dir_entry {
	uint32	inode;			          /* Inode number */
	uint16	rec_len;		          /* Directory entry length */
	uint16	name_len;		          /* Name length */
	char	  name[EXT2_NAME_LEN];	/* File name */
};


void ext2_readsb(int dev, struct ext2_super_block *sb);
void ext2_iinit(int dev);
void ext2_get_dirent(struct inode *dir_ip, char *name, struct ext2_dir_entry *de, int isprev, uint *poff);


#define NBGDESC 10
#define EXT2_ROOTINO 2

// Inodes per block.
#define IPB           (BSIZE / sizeof(struct dinode))

// Block containing inode i
// #define IBLOCK(i, sb)     ((i) / IPB + sb.inodestart)

// Bitmap bits per block
#define BPB           (BSIZE*8)

// Block of free map containing bit for block b
#define EXT2_BBLOCK(b, bg_desc) (b/BPB + bg_desc.bg_inode_bitmap)

struct inode* ext2_iget(uint dev, uint inum);

#define	__S_IFMT	0170000	/* These bits determine file type.  */
#define	__S_IFDIR	0040000	/* Directory.  */
#define	__S_IFREG	0100000	/* Regular file.  */

#define	__S_ISTYPE(mode, mask)	(((mode) & __S_IFMT) == (mask))

#define	ISDIR(mode)	 __S_ISTYPE((mode), __S_IFDIR)
#define	ISREG(mode)	 __S_ISTYPE((mode), __S_IFREG)

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

void          ext2_iupdate(struct inode *ip);
void          ext2_itrunc(struct inode *ip);
struct inode* ext2_iget(uint dev, uint inum);
void          ext2_iput(struct inode *ip);
struct inode* ext2_idup(struct inode *ip);
int           ext2_readi(struct inode *ip, char *dst, uint off, uint n);
int           ext2_writei(struct inode *ip, char *src, uint off, uint n);
int           ext2_dirlink(struct inode *dp, char *name, uint inum);
struct inode* ext2_dirlookup(struct inode *dp, char *name, uint *poff);
void          ext2_ilock(struct inode *ip);
void          ext2_iunlock(struct inode *ip);
void          ext2_iunlockput(struct inode *ip);

#endif /* EXT2_FS_H_ */
