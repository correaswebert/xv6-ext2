#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "xv6_fs.h"
#include "ext2_fs.h"

char*
fmtname(char *path)
{
  static char buf[DIRSIZ+1];
  char *p;

  // Find first character after last slash.
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
    ;
  p++;

  // Return blank-padded name.
  if(strlen(p) >= DIRSIZ)
    return p;
  memmove(buf, p, strlen(p));
  memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  return buf;
}

void
xv6_ls_dir(int fd, struct stat st, char *buf, char *p)
{
  struct dirent de;
  while(read(fd, &de, sizeof(de)) == sizeof(de)){
    if(de.inum == 0)
      continue;
    memmove(p, de.name, DIRSIZ);
    p[DIRSIZ] = 0;
    if(stat(buf, &st) < 0){
      printf(1, "ls: cannot stat %s\n", buf);
      continue;
    }
    printf(1, "%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);
  }
}

void
ext2_ls_dir(int fd, struct stat st, char *buf, char *p)
{
  struct ext2_dir_entry ext2_de;
  uint sum_rec_len = 0;
  char name[256];

  while(1){
    read(fd, &ext2_de, 8);

    sum_rec_len += ext2_de.rec_len;
    if(sum_rec_len >= 2048)
      read(fd, name, ext2_de.name_len);
    else
      read(fd, name, ext2_de.rec_len - 8);
    name[ext2_de.name_len] = '\0';

    memmove(p, name, DIRSIZ);
    p[DIRSIZ] = 0;

    if(stat(buf, &st) < 0){
      printf(1, "ls: cannot stat %s\n", buf);
      continue;
    }

    printf(1, "%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);

    if (sum_rec_len >= 2048)
      break;
  }
}

void
ls(char *path)
{
  char buf[512], *p;
  int fd;
  struct stat st;

  if((fd = open(path, 0)) < 0){
    printf(2, "ls: cannot open %s\n", path);
    return;
  }

  if(fstat(fd, &st) < 0){
    printf(2, "ls: cannot stat %s\n", path);
    close(fd);
    return;
  }
  
  switch(st.type){
  case T_FILE:
    printf(1, "%s %d %d %d\n", fmtname(path), st.type, st.ino, st.size);
    break;

  case T_DIR:
    if(strlen(path) + 1 + DIRSIZ + 1 > sizeof buf){
      printf(1, "ls: path too long\n");
      break;
    }
    strcpy(buf, path);
    p = buf+strlen(buf);
    *p++ = '/';
    if (st.dev == 2)
      ext2_ls_dir(fd, st, buf, p);
    else
      xv6_ls_dir(fd, st, buf, p);

    break;
  }

  close(fd);
}

int
main(int argc, char *argv[])
{
  int i;

  if(argc < 2){
    ls(".");
    exit();
  }
  for(i=1; i<argc; i++)
    ls(argv[i]);
  exit();
}
