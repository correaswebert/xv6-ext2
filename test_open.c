#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "vfs.h"
#include "xv6_fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

char buf[8192];
char name[3];
char *echoargv[] = { "echo", "ALL", "TESTS", "PASSED", 0 };
int stdout = 1;

int
main(int argc, char *argv[])
{
  int fd;
  int i;

  printf(stdout, "small file test\n");

  fd = open("/mnt/small", O_CREATE|O_RDWR);
  if(fd >= 0){
    printf(stdout, "creat small succeeded; ok\n");
  } else {
    printf(stdout, "error: creat small failed!\n");
    exit();
  }

  for(i = 0; i < 100; i++){
    if(write(fd, "aaaaaaaaaa", 10) != 10){
      printf(stdout, "error: write aa %d new file failed\n", i);
      exit();
    }
    if(write(fd, "bbbbbbbbbb", 10) != 10){
      printf(stdout, "error: write bb %d new file failed\n", i);
      exit();
    }
  }
  printf(stdout, "writes ok\n");
  close(fd);

  fd = open("/mnt/small", O_RDONLY);
  if(fd >= 0){
    printf(stdout, "open small succeeded ok\n");
  } else {
    printf(stdout, "error: open small failed!\n");
    exit();
  }

  i = read(fd, buf, 2000);
  if(i == 2000){
    printf(stdout, "read succeeded ok\n");
  } else {
    printf(stdout, "read failed\n");
    exit();
  }
  close(fd);

  printf(stdout, "starting unlinking /mnt/small\n");

  // if(unlink("/mnt/small") < 0){
  //   printf(stdout, "unlink small failed\n");
  //   exit();
  // }
  
  printf(stdout, "small file test ok\n");

  exit();
}