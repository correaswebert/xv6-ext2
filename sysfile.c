//
// File-system system calls.
// Mostly argument checking, since we don't trust
// user code, and calls into file.c and fs.c.
//

#include "types.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "mmu.h"
#include "proc.h"
#include "vfs.h"
#include "xv6_fs.h"
#include "ext2_fs.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "file.h"
#include "fcntl.h"

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
    return -1;
  if(pfd)
    *pfd = fd;
  if(pf)
    *pf = f;
  return 0;
}

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
  int fd;
  struct proc *curproc = myproc();

  for(fd = 0; fd < NOFILE; fd++){
    if(curproc->ofile[fd] == 0){
      curproc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
}

int
sys_dup(void)
{
  struct file *f;
  int fd;

  if(argfd(0, 0, &f) < 0)
    return -1;
  if((fd=fdalloc(f)) < 0)
    return -1;
  filedup(f);
  return fd;
}

int
sys_read(void)
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
    return -1;
  return fileread(f, p, n);
}

int
sys_write(void)
{
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
    return -1;
  return filewrite(f, p, n);
}

int
sys_close(void)
{
  int fd;
  struct file *f;

  if(argfd(0, &fd, &f) < 0)
    return -1;
  myproc()->ofile[fd] = 0;
  fileclose(f);
  return 0;
}

int
sys_fstat(void)
{
  struct file *f;
  struct stat *st;

  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
    return -1;
  return filestat(f, st);
}

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
    return -1;

  begin_op();
  if((ip = namei(old)) == 0){
    end_op();
    return -1;
  }

  ilock(ip);

  if(get_inode_type(ip) == T_DIR){
    iunlockput(ip);
    end_op();
    return -1;
  }

  update_link_count(ip, 1);
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
  ilock(dp);
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
    iunlockput(dp);
    goto bad;
  }
  iunlockput(dp);
  iput(ip);

  end_op();

  return 0;

bad:
  ilock(ip);
  update_link_count(ip, -1);
  iupdate(ip);
  iunlockput(ip);
  end_op();
  return -1;
}

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;
  struct ext2_dir_entry ext2_de;
  int size_dirents;

  if(dp->dev == ROOTDEV){
    size_dirents = ((struct xv6_inode*)dp->fs_inode)->size;

    for(off = 2 * sizeof(de); off < size_dirents; off += sizeof(de)){
      if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
        panic("isdirempty: readi");
      if(de.inum != 0)
        return 0;
    }
  }

  else if(dp->dev == EXT2DEV){
    size_dirents = ((struct ext2_inode*)dp->fs_inode)->i_size;

    // if (rec_len + 12) of .. is BSIZE i.e (2048)
    // then dir is empty as padding in last dirent (..)
    if(readi(dp, (char*)&ext2_de, 12, sizeof(ext2_de)) != sizeof(ext2_de))
      panic("isdirempty: readi");
    if (ext2_de.rec_len + 12 != BSIZE)
      return 0;
  }

  return 1;
}

//PAGEBREAK!
int
sys_unlink(void)
{
  struct inode *ip, *dp;
  struct dirent de;
  struct ext2_dir_entry ext2_de;
  char name[DIRSIZ], *path;
  uint off;
  ushort rec_len;

  if(argstr(0, &path) < 0)
    return -1;

  begin_op();
  if((dp = nameiparent(path, name)) == 0){
    end_op();
    return -1;
  }
  
  ilock(dp);

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0){
    goto bad;
  }

  if((ip = dirlookup(dp, name, &off)) == 0){
    goto bad;
  }

  ilock(ip);

  if(update_link_count(ip, 0) < 1)
    panic("unlink: nlink < 1");
  if(get_inode_type(ip) == T_DIR && !isdirempty(ip)){
    iunlockput(ip);
    goto bad;
  }

  if (dp->dev == ROOTDEV){
    memset(&de, 0, sizeof(de));
    if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("unlink: writei");
  }
  else if (dp->dev == EXT2DEV){
    ext2_get_dirent(dp, name, &ext2_de, 0, &off);
    rec_len = ext2_de.rec_len; // with padding

    ushort x = (ushort)ext2_de.name_len / 4;
    if(ext2_de.name_len % 4 != 0)
      x += 1;
    ext2_de.rec_len = 8 + x*4;
    if(off % BSIZE < ext2_de.rec_len){
      memset(ext2_de.name, -1, ext2_de.name_len);
      if(writei(dp, (char*)&ext2_de, off, rec_len) != rec_len)
        panic("unlink: writei");
    }
    else{
      ext2_get_dirent(dp, name, &ext2_de, 1, &off);
      rec_len += ext2_de.rec_len; // add padding to prev record
      ext2_de.rec_len = rec_len;

      if(writei(dp, (char*)&ext2_de, off, rec_len) != rec_len)
        panic("unlink: writei");
    }
  }

  
  if(get_inode_type(ip) == T_DIR){
    update_link_count(dp, -1);
    iupdate(dp);
  }
  iunlockput(dp);

  update_link_count(ip, -1);
  iupdate(ip);
  iunlockput(ip);

  end_op();

  return 0;

bad:
  iunlockput(dp);
  end_op();
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
  struct inode *ip, *dp;
  char name[256];


  if((dp = nameiparent(path, name)) == 0) {
    return 0;
  }
  
  ilock(dp);

  if((ip = dirlookup(dp, name, 0)) != 0){
    iunlockput(dp);
    ilock(ip);
    if(type == T_FILE && get_inode_type(ip) == T_FILE)
      return ip;
    iunlockput(ip);
    return 0;
  }

  if (dp->dev == 1){
    if((ip = ialloc(dp->dev, type)) == 0)
      panic("create: ialloc");
  }
  else if (dp->dev == 2){
    if((ip = ext2_ialloc(dp->dev, type)) == 0)
      panic("create: ialloc");
  }

  ilock(ip);

  struct xv6_inode *pxv6_inode;
  struct ext2_inode *pext2_inode;
  switch(ip->dev) {
    case ROOTDEV:
      pxv6_inode = ip->fs_inode;
      pxv6_inode->major = major;
      pxv6_inode->minor = minor;
      pxv6_inode->nlink = 1;
      break;

    case EXT2DEV:
      pext2_inode = ip->fs_inode;
      pext2_inode->i_links_count = 1;
      break;
    
    default:
      break;
  }

  iupdate(ip);

  if(type == T_DIR){  // Create . and .. entries.
    update_link_count(dp, 1);  // for ".."
    iupdate(dp);
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
      panic("create dots");
  }

  if(dirlink(dp, name, ip->inum) < 0)
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}

int
sys_open(void)
{
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
    return -1;

  begin_op();

  if(omode & O_CREATE){
    ip = create(path, T_FILE, 0, 0);
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
      end_op();
      return -1;
    }

    ilock(ip);
  
    if(get_inode_type(ip) == T_DIR && omode != O_RDONLY){
      iunlockput(ip);
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
    if(f)
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
  end_op();

  f->type = FD_INODE;
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
  return fd;
}

int
sys_mkdir(void)
{
  char *path;
  struct inode *ip;

  begin_op();
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
    end_op();
    return -1;
  }
  iunlockput(ip);
  end_op();
  return 0;
}

int
sys_mknod(void)
{
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
  if((argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
    end_op();
    return -1;
  }
  iunlockput(ip);
  end_op();
  return 0;
}

int
sys_chdir(void)
{
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
  
  begin_op();
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
    end_op();
    return -1;
  }

  ilock(ip);
  
  if(get_inode_type(ip) != T_DIR){
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
  iput(curproc->cwd);
  end_op();
  curproc->cwd = ip;
  return 0;
}

int
sys_exec(void)
{
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
}

int
sys_pipe(void)
{
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
    return -1;
  if(pipealloc(&rf, &wf) < 0)
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
    if(fd0 >= 0)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
  fd[1] = fd1;
  return 0;
}

int
sys_lseek(void)
{
  int fd, offset, start;
  struct file *pf;
  uint file_size;

  begin_op();

  if(argfd(0, &fd, &pf) < 0 || argint(1, &offset) < 0 || argint(2, &start) < 0) {
    return -1;
  }

  if(pf->ip->dev == ROOTDEV)
    file_size = ((struct xv6_inode*)pf->ip->fs_inode)->size;
  else
    file_size = ((struct ext2_inode*)pf->ip->fs_inode)->i_size;
  
  if(start == SEEK_SET) {
    pf->off = offset;
  }

  else if(start == SEEK_CUR) {
    pf->off += offset;
  }

  else if(start == SEEK_END) {
    if(offset < 0) {
      pf->off = file_size + offset;
    }
    else {
      pf->off = file_size;
    }
  }

  end_op();
  
  return (pf->off);
}
