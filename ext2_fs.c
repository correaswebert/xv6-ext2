#include "defs.h"
#include "param.h"
#include "vfs.h"
#include "ext2_fs.h"
#include "buf.h"
#include "stat.h"

struct ext2_super_block ext2_sb;

struct ext2_icache
{
  struct spinlock lock;
  struct inode inodes[NINODE];
  struct ext2_inode ext2_inodes[NINODE];
} ext2_icache;

struct bgcache
{
  struct spinlock lock;
  int valid;
  struct ext2_group_desc ext2_bgdesc;
} bgcache;

void ext2_check_iops()
{
  cprintf("ext2 iops\n");
}

/**
 * the ext2 FS operations mapping to be used from the VFS generic inode.
 */
struct inode_ops ext2_ops = {
    .iupdate = &ext2_iupdate,
    .itrunc = &ext2_itrunc,
    .iget = &ext2_iget,
    .iput = &ext2_iput,
    .idup = &ext2_idup,
    .readi = &ext2_readi,
    .writei = &ext2_writei,
    .dirlink = &ext2_dirlink,
    .dirlookup = &ext2_dirlookup,
    .ilock = &ext2_ilock,
    .iunlock = &ext2_iunlock,
    .iunlockput = &ext2_iunlockput,
    .check_iops = &ext2_check_iops,
};

/**
 * read the superblock from the device.
 * 
 * @param dev storage device number
 * @param sb pointer to the global superblock variable
 */
void ext2_readsb(int dev, struct ext2_super_block *sb)
{
  struct buf *bp;

  bp = bread(dev, 0);
  memmove(sb, (bp->data) + 1024, sizeof(*sb));
  brelse(bp);
}

/**
 * initlialize the FS by reading superblock from the device.
 * 
 * @param dev storage device number
 */
void ext2_iinit(int dev)
{
  ext2_readsb(dev, &ext2_sb);
  cprintf("sb: magic %x icount = %d bcount = %d\n log block size  %d inodes per group  %d first inode %d \
inode size %d\n",
          ext2_sb.s_magic, ext2_sb.s_inodes_count, ext2_sb.s_blocks_count,
          ext2_sb.s_log_block_size, ext2_sb.s_inodes_per_group, ext2_sb.s_first_ino, ext2_sb.s_inode_size);
}

/**
 * return the block number the given inode (via the inode number) belongs to.
 * 
 * @param inum inode number
 * 
 * @returns block number to which the inode belongs.
 */
static uint64
get_inode_block_num(uint inum)
{
  uint32 local_inode_index = (inum - 1) % ext2_sb.s_inodes_per_group;

  if (!bgcache.valid)
  {
    struct buf *bp = bread(EXT2DEV, 1); // bgdesc in 2nd block
    memmove(&bgcache.ext2_bgdesc, bp->data, sizeof(bgcache.ext2_bgdesc));
    brelse(bp);
    bgcache.valid = 1;
  }

  uint64 inode_offset = ((uint64)bgcache.ext2_bgdesc.bg_inode_table * BSIZE) +
                        (local_inode_index * ext2_sb.s_inode_size);
  return inode_offset;
}

static uint
get_inode_bno(struct inode *ip)
{
  // suppose inum=17 belongs to bno=2
  // if we only do inum % BSIZE, it still remains 17
  // to make it 0 (the local offset inside the block read in buffer)
  // we multiply by inode size to get the exact size in bytes
  // which we can then apply % BSIZE to, and bring back the overflow
  // back to zero.
  // we divide by inode size again to get in terms of inode number
  return get_inode_block_num(ip->inum) / BSIZE;
}

static uint
get_inode_off(struct inode *ip)
{
  uint i_loc_off = ((ip->inum * ext2_sb.s_inode_size) % BSIZE) / ext2_sb.s_inode_size;
  return (((i_loc_off - 1) * 128) % 2048) / 128;
}

/**
 * Zero the block specified.
 * 
 * @param dev storage device number
 * @param bno block number
 */
static void
bzero(int dev, int bno)
{
  struct buf *bp;
  bp = bread(dev, bno);
  memset(bp->data, 0, BSIZE);
  // log_write(bp);
  bwrite(bp);
  brelse(bp);
}

/**
 * allocate a zeroed disk block from the device.
 * 
 * @param dev storage device number
 * 
 * @returns block number of the allocated data block
 */
static uint
balloc(uint dev)
{
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for (b = 0; b < ext2_sb.s_blocks_count; b += BPB)
  {
    bp = bread(dev, bgcache.ext2_bgdesc.bg_block_bitmap);
    for (bi = 0; bi < BPB && b + bi < ext2_sb.s_blocks_count; bi++)
    {
      m = 1 << (bi % 8);
      if ((bp->data[bi / 8] & m) == 0)
      {                        // Is block free?
        bp->data[bi / 8] |= m; // Mark block in use.
        bwrite(bp);
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}

// // Free a disk block.
static void
bfree(int dev, uint b)
{
  struct buf *bp;
  int bi, m;

  bp = bread(dev, bgcache.ext2_bgdesc.bg_block_bitmap);
  bi = b % BPB;
  m = 1 << (bi % 8);
  if ((bp->data[bi / 8] & m) == 0)
    panic("freeing free block");
  bp->data[bi / 8] &= ~m;
  // log_write(bp);
  bwrite(bp);
  brelse(bp);
}

/**
 * return the disk block number of the nth block in inode block array.
 * if there is no such block used, allocate one.
 * 
 * @param ip pointer to generic inode to be mapped
 * @param bn index of the direct blocks
 * 
 * @returns disk block number of the nth data block in inode array
 */
static uint
bmap(struct inode *ip, uint bn)
{
  uint addr, *a;
  struct buf *bp;
  struct ext2_inode *pext2_inode = ip->fs_inode;

  if (bn < NDIRECT)
  {
    if ((addr = pext2_inode->i_block[bn]) == 0)
      pext2_inode->i_block[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;

  if (bn < NINDIRECT)
  {
    // Load indirect block, allocating if necessary.
    if ((addr = pext2_inode->i_block[NDIRECT]) == 0)
      pext2_inode->i_block[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
    a = (uint *)bp->data;
    if ((addr = a[bn]) == 0)
    {
      a[bn] = addr = balloc(ip->dev);
      bwrite(bp);
    }
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}

struct inode *
ext2_ialloc(uint dev, short type)
{
  int b, bi, m;
  struct buf *bp;
  struct inode ip;
  struct ext2_inode *dip;

  bp = 0;
  for (b = 0; b < ext2_sb.s_inodes_count; b += BPB)
  {
    bp = bread(dev, bgcache.ext2_bgdesc.bg_inode_bitmap);
    for (bi = 11; bi < BPB && b + bi < ext2_sb.s_inodes_count; bi++)
    {
      m = 1 << (bi % 8);
      if ((bp->data[bi / 8] & m) == 0)
      {                        // Is block free?
        bp->data[bi / 8] |= m; // Mark block in use.
        bwrite(bp);
        brelse(bp);

        uint inum = b + bi;
        ip.inum = inum;
        bp = bread(dev, get_inode_bno(&ip));
        dip = (struct ext2_inode *)bp->data + get_inode_off(&ip);

        if (dip->i_mode == 0)
        { // a free inode
          memset(dip, 0, sizeof(*dip));

          if (type == T_FILE)
            dip->i_mode = 0x81ff;
          if (type == T_DIR)
            dip->i_mode = 0x41ff;

          bwrite(bp); // mark it allocated on the disk
          brelse(bp);
          return ext2_iget(dev, inum);
        }
      }
    }
    brelse(bp);
  }
  panic("ext2_ialloc: out of inodes");
}

// Lock the given inode.
// Reads the inode from disk if necessary.
void ext2_ilock(struct inode *ip)
{
  struct buf *bp;
  struct ext2_inode *dip;

  if (ip == 0 || ip->ref < 1)
    panic("ilock");

  acquiresleep(&ip->lock);

  if (ip->valid == 0)
  {
    bp = bread(ip->dev, get_inode_bno(ip));
    dip = (struct ext2_inode *)bp->data + get_inode_off(ip);

    struct ext2_inode *pext2_inode = ip->fs_inode;
    memmove(pext2_inode, dip, sizeof(struct ext2_inode));
    brelse(bp);
    ip->valid = 1;
    if (pext2_inode->i_mode == 0)
      panic("ilock: no type");
  }
}

// Unlock the given inode.
void ext2_iunlock(struct inode *ip)
{
  if (ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
    panic("iunlock");

  releasesleep(&ip->lock);
}

// Common idiom: unlock, then put.
void ext2_iunlockput(struct inode *ip)
{
  iunlock(ip);
  ext2_iput(ip);
}

// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
struct inode *
ext2_iget(uint dev, uint inum)
{
  struct inode *ip, *empty;
  acquire(&ext2_icache.lock);

  // Is the inode already cached?
  int inode_num = -1;
  empty = 0;
  for (ip = &ext2_icache.inodes[0]; ip < &ext2_icache.inodes[NINODE]; ip++)
  {
    if (!empty)
      inode_num++;

    if (ip->ref > 0 && ip->dev == dev && ip->inum == inum)
    {
      ip->ref++;
      release(&ext2_icache.lock);
      return ip;
    }

    // Remember empty slot.
    if (empty == 0 && ip->ref == 0)
      empty = ip;
  }

  // Recycle an inode cache entry.
  if (empty == 0)
    panic("ext2_iget: no inodes");

  // empty now contains a reference to the free inode found in cache
  ip = empty;

  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  ip->iops = &ext2_ops;
  ip->fs_inode = &ext2_icache.ext2_inodes[inode_num];

  release(&ext2_icache.lock);
  return ip;
}

/**
 * read data from data block, starting from the offset given pointed by
 * the inode into the destination. Caller must hold ip->lock.
 * 
 * @param ip inode pointer
 * @param dst destination address as char *
 * @param off offset from where to begin reading
 * @param nbytes number of bytes to be read into the dst
 * 
 * @returns the number of bytes read
 */
int ext2_readi(struct inode *ip, char *dst, uint off, uint nbytes)
{
  uint bytes_read, num_bytes;
  struct buf *bp;
  struct ext2_inode *pext2_inode = ip->fs_inode;

  if (off > pext2_inode->i_size)
  {
    cprintf("ext2_readi: failed -1\n");
    return -1;
  }
  // 2048 + 264 > 2048
  if (off + nbytes > pext2_inode->i_size)
    nbytes = pext2_inode->i_size - off;

  for (bytes_read = 0;
       bytes_read < nbytes;
       bytes_read += num_bytes, off += num_bytes, dst += num_bytes)
  {
    bp = bread(ip->dev, bmap(ip, off / BSIZE));
    num_bytes = min(nbytes - bytes_read, BSIZE - off % BSIZE);
    memmove(dst, bp->data + off % BSIZE, num_bytes);
    brelse(bp);
  }
  return nbytes;
}

// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int ext2_writei(struct inode *ip, char *src, uint off, uint n)
{
  uint tot, m;
  struct buf *bp;
  struct ext2_inode *pext2_inode = ip->fs_inode;

  if (off > pext2_inode->i_size || off + n < off)
  {
    return -1;
  }
  if (off + n > MAXFILE * BSIZE)
    return -1;

  for (tot = 0; tot < n; tot += m, off += m, src += m)
  {
    bp = bread(ip->dev, bmap(ip, off / BSIZE));
    m = min(n - tot, BSIZE - off % BSIZE);
    memmove(bp->data + off % BSIZE, src, m);
    bwrite(bp);
    brelse(bp);
  }

  if (n > 0 && off > pext2_inode->i_size)
  {
    pext2_inode->i_size = off;
    ext2_iupdate(ip);
  }
  return n;
}

// Copy a modified in-memory inode to disk.
// Must be called after every change to an ip->xxx field
// that lives on disk, since i-node cache is write-through.
// Caller must hold ip->lock.
void ext2_iupdate(struct inode *ip)
{
  struct buf *bp;
  struct ext2_inode *dip;

  bp = bread(ip->dev, get_inode_bno(ip));
  dip = (struct ext2_inode *)bp->data + get_inode_off(ip);

  struct ext2_inode *pext2_inode = ip->fs_inode;
  memmove(dip, pext2_inode, sizeof(struct ext2_inode));
  bwrite(bp);
  brelse(bp);
}

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode *
ext2_idup(struct inode *ip)
{
  acquire(&ext2_icache.lock);
  ip->ref++;
  release(&ext2_icache.lock);
  return ip;
}

// Drop a reference to an in-memory inode.
// If that was the last reference, the inode cache entry can
// be recycled.
// If that was the last reference and the inode has no links
// to it, free the inode (and its content) on disk.
// All calls to ext2_iput() must be inside a transaction in
// case it has to free the inode.
void ext2_iput(struct inode *ip)
{
  acquiresleep(&ip->lock);
  struct ext2_inode *pext2_inode = ip->fs_inode;
  if (ip->valid && pext2_inode->i_links_count == 0)
  {
    acquire(&ext2_icache.lock);
    int r = ip->ref;
    release(&ext2_icache.lock);
    if (r == 1)
    {
      // inode has no links and no other references: truncate and free.
      ext2_itrunc(ip);
      pext2_inode->i_mode = 0;
      ext2_iupdate(ip);
      ip->valid = 0;
    }
  }
  releasesleep(&ip->lock);

  acquire(&ext2_icache.lock);
  ip->ref--;
  release(&ext2_icache.lock);
}

// Truncate inode (discard contents).
// Only called when the inode has no links
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
void ext2_itrunc(struct inode *ip)
{
  int i, j;
  struct buf *bp;
  uint *a;
  struct ext2_inode *pext2_inode = ip->fs_inode;

  for (i = 0; i < NDIRECT; i++)
  {
    if (pext2_inode->i_block[i])
    {
      bfree(ip->dev, pext2_inode->i_block[i]);
      pext2_inode->i_block[i] = 0;
    }
  }

  if (pext2_inode->i_block[NDIRECT])
  {
    bp = bread(ip->dev, pext2_inode->i_block[NDIRECT]);
    a = (uint *)bp->data;
    for (j = 0; j < NINDIRECT; j++)
    {
      if (a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
    bfree(ip->dev, pext2_inode->i_block[NDIRECT]);
    pext2_inode->i_block[NDIRECT] = 0;
  }

  pext2_inode->i_size = 0;
  ext2_iupdate(ip);
}

void ext2_get_dirent(struct inode *dir_ip, char *name, struct ext2_dir_entry *de, int isprev, uint *poff)
{
  uint prev_off = 0, off, i, sum_rec_len, b;
  struct ext2_dir_entry prev_de;
  struct ext2_inode *pext2_inode = dir_ip->fs_inode;

  if (!ISDIR(pext2_inode->i_mode))
    panic("ext2_dirlookup: not DIR");

  for (i = 0; i < 12; i++)
  {
    sum_rec_len = 0;

    for (off = i * BSIZE; off < pext2_inode->i_size; off += de->rec_len)
    {
      b = min(BSIZE - off % BSIZE, sizeof(struct ext2_dir_entry));
      if (ext2_readi(dir_ip, (char *)de, off, b) != b)
      {
        panic("ext2_dirlookup read");
      }

      de->name[de->name_len] = '\0';
      if (strncmp(name, de->name, max(strlen(name), strlen(de->name))) == 0)
      {
        // entry matches path element
        if (*poff)
          *poff = off;

        if (isprev)
        {
          *de = prev_de;
          *poff = prev_off;
          return;
        }
        else
          return;
      }
      prev_de = *de;
      prev_off = off;

      sum_rec_len += de->rec_len;
      if (sum_rec_len >= BSIZE)
        break;
    }
  }

  return;
}

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode *
ext2_dirlookup(struct inode *dir_ip, char *name, uint *poff)
{
  uint off, inum, i, b, sum_rec_len;
  struct ext2_dir_entry de;
  struct ext2_inode *pext2_inode = dir_ip->fs_inode;

  if (!ISDIR(pext2_inode->i_mode))
    panic("ext2_dirlookup: not DIR");

  for (i = 0; i < 12; i++)
  {
    sum_rec_len = 0;

    for (off = i * BSIZE; off < pext2_inode->i_size; off += de.rec_len)
    {
      b = min(BSIZE - off % BSIZE, sizeof(struct ext2_dir_entry));
      if (ext2_readi(dir_ip, (char *)&de, off, b) != b)
      {
        panic("ext2_dirlookup read");
      }

      de.name[de.name_len] = '\0';
      if (strncmp(name, de.name, max(strlen(name), de.name_len)) == 0)
      {
        // entry matches path element
        if (poff)
          *poff = off;
        inum = de.inode;
        return ext2_iget(dir_ip->dev, inum);
      }

      sum_rec_len += de.rec_len;
      if (sum_rec_len >= BSIZE)
        break;
    }
  }

  return 0;
}

// Write a new directory entry (name, inum) into the directory dp.
int ext2_dirlink(struct inode *dir_ip, char *name, uint inum)
{
  int off;
  struct ext2_dir_entry de;
  struct inode *ip;
  struct ext2_inode *pext2_inode = dir_ip->fs_inode;

  // Check that name is not present.
  if ((ip = ext2_dirlookup(dir_ip, name, 0)) != 0)
  {
    ext2_iput(ip);
    return -1;
  }

  uint sum_rec_len = 0;
  uint padding = 0, i, b;

  // Look for an empty dirent.
  for (i = 0; i < 12; i++)
  {
    sum_rec_len = 0;
    padding = 0;

    for (off = i * BSIZE; off < pext2_inode->i_size; off += de.rec_len)
    {
      b = min(BSIZE - off % BSIZE, sizeof(struct ext2_dir_entry));
      if (ext2_readi(dir_ip, (char *)&de, off, b) != b)
      {
        panic("ext2_dirlink read (1)");
      }

      sum_rec_len += de.rec_len;

      if (sum_rec_len >= BSIZE)
      {
        padding = de.rec_len; // padding + rec_len
        de.name_len = min(strlen(de.name), de.name_len);

        ushort x = (ushort)de.name_len / 4;
        if (de.name_len % 4 != 0)
          x += 1;
        de.rec_len = 8 + x * 4;

        if (padding <= de.rec_len)
          break;

        padding -= de.rec_len; // pure padding

        if (ext2_writei(dir_ip, (char *)&de, off, de.rec_len) != de.rec_len)
          panic("ext2_dirlink write (1)");

        // go to next dirent
        off += de.rec_len;

        if (off >= pext2_inode->i_size)
        {
          padding = 0;
          break;
        }

        b = min(BSIZE - off % BSIZE, sizeof(struct ext2_dir_entry));
        if (ext2_readi(dir_ip, (char *)&de, off, b) != b)
        {
          panic("ext2_dirlink read (2)");
        }

        // initialize dirent
        strncpy(de.name, name, strlen(name));
        if (strlen(name) % 4 != 0)
          de.name[strlen(name)] = '\0';
        de.inode = inum;
        de.name_len = strlen(name);

        x = (ushort)de.name_len / 4;
        if (de.name_len % 4 != 0)
          x += 1;
        de.rec_len = padding ? padding : 8 + x * 4;

        if (ext2_writei(dir_ip, (char *)&de, off, de.rec_len) != de.rec_len)
          panic("ext2_dirlink write (2)");

        return 0;
      }
    }

    // initialize dirent
    if (padding == 0)
    {
      strncpy(de.name, name, strlen(name));
      if (strlen(name) % 4 != 0)
        de.name[strlen(name)] = '\0';
      de.inode = inum;
      de.name_len = strlen(name);
      de.rec_len = BSIZE;

      if (ext2_writei(dir_ip, (char *)&de, off, de.rec_len) != de.rec_len)
        panic("ext2_dirlink write (3)");

      return 0;
    }
  }

  return -1;
}
