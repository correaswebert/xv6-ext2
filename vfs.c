#include "types.h"
#include "defs.h"
#include "param.h"
#include "stat.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "vfs.h"
#include "xv6_fs.h"
#include "ext2_fs.h"
#include "buf.h"
#include "file.h"

ushort
get_inode_type(struct inode *ip)
{ 
  struct xv6_inode *pxv6_inode;
  struct ext2_inode *pext2_inode;

  switch (ip->dev)
  {
  case ROOTDEV:
    pxv6_inode = ip->fs_inode;
    return pxv6_inode->type;
  
  case EXT2DEV:
    pext2_inode = ip->fs_inode;
    if(ISDIR(pext2_inode->i_mode))
      return T_DIR;
    if(ISREG(pext2_inode->i_mode))
      return T_FILE;
  
  default:
    panic("get_inode_type");
  }
}

short
update_link_count(struct inode *ip, short update_val)
{
  struct xv6_inode *pxv6_inode;
  struct ext2_inode *pext2_inode;

  switch (ip->dev)
  {
  case ROOTDEV:
    pxv6_inode = ip->fs_inode;
    pxv6_inode->nlink += update_val;
    return pxv6_inode->nlink;
  
  case EXT2DEV:
    pext2_inode = ip->fs_inode;
    pext2_inode->i_links_count += update_val;
    return pext2_inode->i_links_count;
  
  default:
    panic("update_link_count");
  }
}

void iupdate(struct inode *ip) {
    ip->iops->iupdate(ip);
}

void itrunc(struct inode *ip) {
    ip->iops->itrunc(ip);
}

void iput(struct inode *ip) {
    ip->iops->iput(ip);
}

int readi(struct inode *ip, char *dst, uint off, uint n) {
    return ip->iops->readi(ip, dst, off, n); 
}

int writei(struct inode *ip, char *src, uint off, uint n) {
    return ip->iops->writei(ip, src, off, n); 
}

int dirlink(struct inode *dp, char *name, uint inum) {
    return dp->iops->dirlink(dp, name, inum);
}

struct inode* dirlookup(struct inode *dp, char *name, uint *poff) {
    return dp->iops->dirlookup(dp, name, poff);
}

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
  return ip->iops->idup(ip);
}

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
  ip->iops->ilock(ip);
}

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
  ip->iops->iunlock(ip);
}

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
  ip->iops->iunlockput(ip);
}

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
  st->dev = ip->dev;
  st->ino = ip->inum;

  if(ip->dev == 1){
    struct xv6_inode *pxv6_inode = ip->fs_inode;
    st->type = pxv6_inode->type;
    st->nlink = pxv6_inode->nlink;
    st->size = pxv6_inode->size;
  }
  else if(ip->dev == 2){
    struct ext2_inode *pext2_inode = ip->fs_inode;
    if (ISDIR(pext2_inode->i_mode))
      st->type = T_DIR;
    else if (ISREG(pext2_inode->i_mode))
      st->type = T_FILE;
    // st->type = pext2_inode->i_mode; // !! apply bit to type conversion logic
    st->nlink = pext2_inode->i_links_count;
    st->size = pext2_inode->i_size;
  }
}

// Paths

// Copy the next path element from path into name.
// Return a pointer to the element following the copied one.
// The returned path has no leading slashes,
// so the caller can check *path=='\0' to see if the name is the last one.
// If no name to remove, return 0.
//
// Examples:
//   skipelem("a/bb/c", name) = "bb/c", setting name = "a"
//   skipelem("///a//bb", name) = "bb", setting name = "a"
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name, uint dev)
{
  char *s;
  int len;
  int dirsiz = (dev == ROOTDEV) ? DIRSIZ : 256;

  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
    path++;
  len = path - s;

  if(len >= dirsiz)
    memmove(name, s, dirsiz);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }

  while(*path == '/')
    path++;
  return path;
}

// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls xv6_iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
  struct inode *ip, *next;

  // cannot use struct inode's iops as ip is not initialised
  // check special condition first ("/mnt" then '/')

  if(strncmp(path, "/mnt/", 5) == 0) {
    ip = ext2_iget(EXT2DEV, EXT2_ROOTINO);
    path += 4; // remove the "/mnt" prefix
  }
  else if(*path == '/'){
    ip = xv6_iget(ROOTDEV, ROOTINO);
  }
  else {
    ip = myproc()->cwd;
    idup(ip);
  }

  while((path = skipelem(path, name, ip->dev)) != 0){
    ilock(ip);

    if((ip->dev == 1 && ((struct xv6_inode *)(ip->fs_inode))->type != T_DIR)
      ||(ip->dev == 2 && !ISDIR(((struct ext2_inode *)ip->fs_inode)->i_mode))){
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
      // Stop one level early.
      iunlock(ip);
      return ip;
    }

    if((next = dirlookup(ip, name, 0)) == 0){
      iunlockput(ip);
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
    iput(ip);
    return 0;
  }
  
  return ip;
}

struct inode*
namei(char *path)
{
  char name[256];
  return namex(path, 0, name);
}

struct inode*
nameiparent(char *path, char *name)
{
  return namex(path, 1, name);
}
