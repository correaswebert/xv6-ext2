#ifndef DATE_H_
#define DATE_H_

struct rtcdate {
  uint second;
  uint minute;
  uint hour;
  uint day;
  uint month;
  uint year;
};

#endif /* DATE_H_ */
